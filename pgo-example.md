# How to do continous profiling

## Setup
Create cluster and deploy stuff
```bash
./kind-cluster-with-registry.sh
```

Add helm chart
```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

Deploy pyroscope
> https://grafana.com/docs/pyroscope/latest/deploy-kubernetes/helm/
```bash
kubectl create namespace pyroscoping
helm -n pyroscoping install pyroscope grafana/pyroscope

# optional with grafana
helm upgrade -n pyroscoping --install grafana grafana/grafana \
  --set image.repository=grafana/grafana \
  --set image.tag=main \
  --set env.GF_FEATURE_TOGGLES_ENABLE=flameGraph \
  --set env.GF_AUTH_ANONYMOUS_ENABLED=true \
  --set env.GF_AUTH_ANONYMOUS_ORG_ROLE=Admin \
  --set env.GF_DIAGNOSTICS_PROFILING_ENABLED=true \
  --set env.GF_DIAGNOSTICS_PROFILING_ADDR=0.0.0.0 \
  --set env.GF_DIAGNOSTICS_PROFILING_PORT=6060 \
  --set-string 'podAnnotations.pyroscope\.grafana\.com/scrape=true' \
  --set-string 'podAnnotations.pyroscope\.grafana\.com/port=6060'
```

## Go app

First of all build and deploy our app
```bash
docker build --build-arg PGO_FILE=off --build-arg BUILD_VERSION=1.0.0 -t localhost:5001/go-pgo-app:1.0.0 .
docker push localhost:5001/go-pgo-app:1.0.0
helm install go-pgo-app ./chart/
```

### If you're using podman

You need to explicitly enable insecure registry
```bash
cat > /etc/containers/registries.conf.d/local.conf <<EOF
[[registry]]
location = "localhost:5001"
insecure = true

EOF
```

We need some workload on our server. Let's send some mds its way.
```bash
kubectl port-forward svc/go-pgo-app-chart 8080:80

# load test
# -q parameter is for query rate (qps)
# -n is for total number of requests
oha -n 2000 -q 500 -m POST -D $(pwd)/awesome.md "http://localhost:8080/render"

# also available
oha -n 2000 -q 500 -m POST "http://localhost:8080/pi?num=10000" # num is number of iterations
oha -n 2000 -q 500 -m POST "http://localhost:8080/fibonacci?num=100000" # num is number of iterations
```

Check pyroscope UI and collect traces
```bash
# open localhost:4040 in your browser to view traces
kubectl --namespace pyroscoping port-forward svc/pyroscope 4040:4040

profilecli query merge --query='{service_name="simple.golang.app", iteration="1.0.0"}' --profile-type="process_cpu:cpu:nanoseconds:cpu:nanoseconds" --from="now-1d" --to="now" --output=pprof=./go-pgo-app-v1.0.0.pgo
```

Update with pgo app (or install new one to test side by side)
and send some other workload and compare results.
```bash
docker build --build-arg PGO_FILE=./go-pgo-app-v1.0.0.pgo --build-arg BUILD_VERSION=1.0.1 -t localhost:5001/go-pgo-app:1.0.1 .
docker push localhost:5001/go-pgo-app:1.0.1

helm upgrade --set image.tag=1.0.1 go-pgo-app ./chart/

# or create new one
helm install --set image.tag=1.0.1 go-pgo-app-v1-0-1 ./chart/
# and port forward to 8081
kubectl port-forward svc/go-pgo-app-v1-0-1-chart 8081:80
oha -n 2000 -q 500 -m POST -D $(pwd)/awesome.md "http://localhost:8081/render"
```

## Cleanup
```
kind delete cluster
docker rm -f kind-registry
```
