package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"runtime/pprof"
	"strconv"

	"gitlab.com/golang-commonmark/markdown"
)

func makeRender() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// these two ways of adding tags are equivalent:
		pprof.Do(r.Context(), pprof.Labels("controller", "render"), func(c context.Context) {
			if r.Method != "POST" {
				http.Error(w, "Only POST allowed", http.StatusMethodNotAllowed)
				return
			}

			src, err := io.ReadAll(r.Body)
			if err != nil {
				log.Printf("error reading body: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			md := markdown.New(
				markdown.XHTMLOutput(true),
				markdown.Typographer(true),
				markdown.Linkify(true),
				markdown.Tables(true),
			)

			var buf bytes.Buffer
			if err := md.Render(&buf, src); err != nil {
				log.Printf("error converting markdown: %v", err)
				http.Error(w, "Malformed markdown", http.StatusBadRequest)
				return
			}

			if _, err := io.Copy(w, &buf); err != nil {
				log.Printf("error writing response: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}
		})
	}
}

func makeFibonacci() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// these two ways of adding tags are equivalent:
		pprof.Do(r.Context(), pprof.Labels("controller", "fibonacci"), func(c context.Context) {
			if r.Method != "POST" {
				http.Error(w, "Only POST allowed", http.StatusMethodNotAllowed)
				return
			}

			n, err := strconv.ParseInt(r.URL.Query().Get("num"), 10, 64)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			res := fibonacci(n)

			_, _ = w.Write([]byte(fmt.Sprintf("%d", res)))
		})
	}
}

func makePi() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// these two ways of adding tags are equivalent:
		pprof.Do(r.Context(), pprof.Labels("controller", "pi"), func(c context.Context) {
			if r.Method != "POST" {
				http.Error(w, "Only POST allowed", http.StatusMethodNotAllowed)
				return
			}

			n, err := strconv.ParseInt(r.URL.Query().Get("num"), 10, 64)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			res := pi(n)

			_, _ = w.Write([]byte(res))
		})
	}
}

// Fibonacci calculates the n-th Fibonacci number using arbitrary-precision arithmetic.
func fibonacci(n int64) *big.Int {
	// Base cases
	if n == 0 {
		return big.NewInt(0)
	}
	if n == 1 {
		return big.NewInt(1)
	}

	// Create big.Int variables to hold Fibonacci numbers
	a := big.NewInt(0)
	b := big.NewInt(1)
	c := new(big.Int)

	// Compute Fibonacci number iteratively
	var i int64 = 2
	for ; i <= n; i++ {
		c.Set(a)    // c = a
		c.Add(c, b) // c = a + b
		a.Set(b)    // a = b
		b.Set(c)    // b = c
	}

	return c
}

// Taken from https://play.golang.org/p/mb5eoZpnYN
// No license specified
func pi(ndigits int64) string {
	if ndigits <= 7 {
		return "3.141595"
	} else {
		digits := big.NewInt(ndigits + 10)
		unity := big.NewInt(0)                 // crea un entero tocho
		unity.Exp(big.NewInt(10), digits, nil) // Le asigna valor
		pi := big.NewInt(0)
		four := big.NewInt(4) // Todos deben ser enteros tocho

		// Serie de McLaurin
		pi.Mul(four, pi.Sub(pi.Mul(four, arccot(5, unity)), arccot(239, unity)))
		output := fmt.Sprintf("%s.%s", pi.String()[0:1], pi.String()[1:ndigits])
		return output
	}
}

func arccot(x int64, unity *big.Int) *big.Int {
	bigx := big.NewInt(x)
	xsquared := big.NewInt(x * x)
	sum := big.NewInt(0)
	sum.Div(unity, bigx)
	xpower := big.NewInt(0)
	xpower.Set(sum)
	n := int64(3)
	zero := big.NewInt(0)
	sign := false

	term := big.NewInt(0)
	for {
		xpower.Div(xpower, xsquared)
		term.Div(xpower, big.NewInt(n))
		if term.Cmp(zero) == 0 {
			break
		}
		if sign {
			sum.Add(sum, term)
		} else {
			sum.Sub(sum, term)
		}
		sign = !sign
		n += 2
	}
	return sum
}
