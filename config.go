package main

import "github.com/spf13/viper"

var Version = "dev"

type Config struct {
	PyroscopeConfig PyroscopeConfig
	ServerConfig    ServerConfig
}

type PyroscopeConfig struct {
	ApplicationName string
	ServerAddress   string
}

type ServerConfig struct {
	Port int
}

func loadConfig() Config {
	confer := viper.New()

	confer.AutomaticEnv()

	return Config{
		PyroscopeConfig: PyroscopeConfig{
			ApplicationName: confer.GetString("PYROSCOPE_APPLICATION_NAME"),
			ServerAddress:   confer.GetString("PYROSCOPE_SERVER_ADDRESS"),
		},
		ServerConfig: ServerConfig{
			Port: confer.GetInt("SERVER_PORT"),
		},
	}
}
