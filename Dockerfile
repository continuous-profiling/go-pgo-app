# syntax=docker/dockerfile:1

##
## Build
##
FROM golang:1.23.0-bookworm AS build

ARG BUILD_VERSION
ARG PGO_FILE

WORKDIR /

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -pgo=$PGO_FILE -ldflags="-X main.Version=${BUILD_VERSION}" -o /server

##
## Deploy
##
FROM gcr.io/distroless/base-debian12

WORKDIR /

EXPOSE 8080

USER nonroot:nonroot
COPY --from=build /server /server

ENTRYPOINT ["/server"]
