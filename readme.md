# Salam Dunya

## Tools used in this demo
- [Helm](https://helm.sh/docs/intro/install/)
- [ohayou](https://github.com/hatoo/oha)
- [kind](https://kind.sigs.k8s.io/docs/user/quick-start)
- [profilecli](https://grafana.com/docs/pyroscope/latest/view-and-analyze-profile-data/profile-cli/)

## Build Requirements
Specify version when building with ldflags. This will be used as iteration tag for Pyroscope.
```bash
go build -ldflags "-X main.Version=1.0.0"
```

## Docker build
You need to pass the version in docker build via build-args
```bash
export VERSION=1.0.0
docker build --build-arg BUILD_VERSION=$VERSION -t localhost:5001/go-pgo-app:$VERSION .

# if you're using local registry
docker push localhost:5001/go-pgo-app:$VERSION
```

## Environment variables
```yaml
envs:
  - name: PYROSCOPE_APPLICATION_NAME
    value: simple.golang.app
  - name: PYROSCOPE_SERVER_ADDRESS
    value: http://pyroscope.pyroscoping.svc.cluster.local.:4040
  - name: SERVER_PORT
    value: 8080
  - name: HOSTNAME
    value: go-pgo-app
```

## Load test
```bash
oha -n 2000 -m POST -D $(pwd)/awesome.md "http://localhost:8080/render"
```

## Read next
[./pgo-example.md](./pgo-example.md)
