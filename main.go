package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"runtime"

	"github.com/grafana/pyroscope-go"
)

func main() {
	config := loadConfig()

	// These 2 lines are only required if you're using mutex or block profiling
	// Read the explanation below for how to set these rates:
	runtime.SetMutexProfileFraction(5)
	runtime.SetBlockProfileRate(5)

	profiler, err := pyroscope.Start(pyroscope.Config{
		ApplicationName: config.PyroscopeConfig.ApplicationName,

		// replace this with the address of pyroscope server
		ServerAddress: config.PyroscopeConfig.ServerAddress,

		// you can disable logging by setting this to nil
		Logger: pyroscope.StandardLogger,

		// you can provide static tags via a map:
		Tags: map[string]string{
			"hostname":  os.Getenv("HOSTNAME"),
			"iteration": Version,
		},

		ProfileTypes: []pyroscope.ProfileType{
			// these profile types are enabled by default:
			pyroscope.ProfileCPU,
			pyroscope.ProfileAllocObjects,
			pyroscope.ProfileAllocSpace,
			pyroscope.ProfileInuseObjects,
			pyroscope.ProfileInuseSpace,

			// these profile types are optional:
			pyroscope.ProfileGoroutines,
			pyroscope.ProfileMutexCount,
			pyroscope.ProfileMutexDuration,
			pyroscope.ProfileBlockCount,
			pyroscope.ProfileBlockDuration,
		},
	})
	if err != nil {
		panic(err)
	}
	_ = profiler

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { _, _ = w.Write([]byte("OK")) })
	http.HandleFunc("/render", makeRender())
	http.HandleFunc("/pi", makePi())
	http.HandleFunc("/fibonacci", makeFibonacci())

	log.Printf("Serving on port %d...", config.ServerConfig.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.ServerConfig.Port), nil))
}
